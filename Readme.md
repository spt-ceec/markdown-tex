# Información

<https://github.com/fletcher/MultiMarkdown>

# Instalación

Descargamos la última versión de Multimarkdown para nuestra plataforma:

<http://fletcherpenney.net/multimarkdown/download/>

Para OS X, el fichero necesarios es él [Mac Installer](http://files.fletcherpenney.net.s3.amazonaws.com/MultiMarkdown-Mac-4.7.1.dmg)

Una vez instalado los ejecutables del Multimarkdown, ya podemos clocar este repositorio.

<https://bitbucket.org/spt-ceice/markdown-tex/>

Para clonar el repositorio podemos usar el siguiente comando:

	git clone https://rbreton@bitbucket.org/spt-ceice/markdown-tex.git

## Opcional

Si queremos tener las características de LATEX en todos los documentos `.md` sin necesidad de clonar el repositorio debemos instalar en  `~/Library/texmf/tex/latex/mmd` los siguientes ficheros:

<https://github.com/fletcher/peg-multimarkdown-latex-support>

# Sintaxis


	$ multimarkdown --help
	
	MultiMarkdown version 4.7.1
	
	    multimarkdown  [OPTION...] [FILE...]
	
	    Options:
	    -h, --help             Show help
	    -v, --version          Show version information
	    -o, --output=FILE      Send output to FILE
	    -t, --to=FORMAT        Convert to FORMAT
	    -b, --batch            Process each file separately
	    -c, --compatibility    Markdown compatibility mode
	    -f, --full             Force a complete document
	    -s, --snippet          Force a snippet
	    --process-html         Process Markdown inside of raw HTML
	    -m, --metadata-keys    List all metadata keys
	    -e, --extract          Extract specified metadata
	    --random               Use random numbers for footnote anchors
	
	    -a, --accept           Accept all CriticMarkup changes
	    -r, --reject           Reject all CriticMarkup changes
	
	    --smart, --nosmart     Toggle smart typography
	    --notes, --nonotes     Toggle footnotes
	    --labels, --nolabels   Disable id attributes for headers
	    --mask, --nomask       Mask email addresses in HTML
	    --escaped-line-breaks  Enable escaped line breaks
	
	Available FORMATs: html(default), latex, beamer, memoir, odf, opml, lyx, mmd



# LATEX:

	multimarkdown --to=latex --output=Documento_xxx.tex --accept Documento_xxx.md

Si el fichero not tiene [CriticMarkup](http://criticmarkup.com) entonces no es necesario aceptar todos los cambios:

	multimarkdown --to=latex --output=Documento_xxx.tex  Documento_xxx.md




## Metadata:



	Autor: <Autor>  
	Title: <Título>  
	Base Header Level: 1  
	latex input: tex/mmd-article-header  
	Base Header Level: 3  
	LaTeX Mode: article  
	latex input: tex/mmd-article-begin-doc  
	latex footer: tex/mmd-article-footer  


Observaciones:
* Debe ir al principio
* Para evitar problemas, debe tener dos espacios al final de cada línea
* `Base Header Level:` indica el nivel `latex` que se corresponde con `H`
	+ 1: Parte
	+ 2: Capítulo
	+ 4: Sección (recomendable)
	
Para que funcione la compilación en `latex`, se deben incluir los siguientes ficheros en el directorio `tex` del presente repositorio. Dichos ficheros también pueden descargarse directamente de:

<https://www.dropbox.com/sh/sfq9rf1e50cqgjy/AACwhFPJT9C7yelzkfYns6Eka?dl=0>



# ODF:


	multimarkdown --to=odf --output=Documento_xxx.odf --accept Documento_xxx.md


Si el fichero not tiene [CriticMarkup](http://criticmarkup.com) entonces no es necesario aceptar todos los cambios:

	multimarkdown --to=odf --output=Documento_xxx.odf  Documento_xxx.md


## Metadata:

	Title: <Título>  
	Base Header Level: 3  

Observaciones:
* Debe ir al principio
* Para evitar problemas, debe tener dos espacios al final de cada línea
* `Base Header Level:` indica el nivel de título que se va a otorgar al símbolo `#`